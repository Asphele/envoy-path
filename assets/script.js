$(document).ready(function() {
  var skillboxes = document.querySelectorAll(".box");
  var reset_button = document.querySelector(".reset");
  var sp = document.querySelector(".sp");

  class skill {
    constructor(id, box, active) {
      this.id = id;
      this.box = box;
      this.active = active;
    }
  }

  var level = 10;
  var total = [];
  var index = 0;

  function createSkill(id, box, active) {
    var a = new skill(id, box, active);
    total.push(a);
  }

  skillboxes.forEach(e => {
    let currentIndex = index;
    if (currentIndex == index) {
      index += 1;
    }
    createSkill(index, e, false);
  });

  total.forEach(e => {
    $(e.box).click(function() {
      check(e);
    });

    $(reset_button).click(function() {
      e.active = false;
      e.box.style.border = "2px solid White";
      level = 10;
    });
  });

  function check(e) {
    if (e.active == false && level > 0) {
      e.active = true;
      e.box.style.border = "2px solid Red";
      level -= 1;
    } else if (level < 0 && e.active == true) {
      e.active = false;
      e.box.style.border = "2px solid White";
      level += 1;
    } else if (level <= 0 && e.active == false) {
      alert("No sp left");
    } else {
      e.active = false;
      e.box.style.border = "2px solid White";
      level += 1;
    }
    $(sp).text(level);
    console.log(e);
  }

  function updateScreen(currentLevel) {
    setInterval(function() {
      currentLevel = level;
      $(sp).text(currentLevel);
    }, 1000 / 60);
  }

  updateScreen(level);
});

while (screenTop > 1080) {}
